package com.spaceflight.presentation.flightlist


import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.Espresso.pressBack
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.idling.CountingIdlingResource
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.domain.model.FlightDto
import com.spaceflight.R
import com.spaceflight.common.utils.EspressoIdlinResource
import com.spaceflight.presentation.MainActivity
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class FlightListFragmentTest {

    @get :Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)



    @Before
    fun setUp() {
        IdlingRegistry.getInstance().register(EspressoIdlinResource.countingIdlingResource)


    }

    @Test
    fun test_isListFragmentVisible_onAppLaunch(){
        onView(withId(R.id.flightRcv)).check(matches(isDisplayed()))
    }


    @Test
    fun test_selectItem_isDetailFragmentVisible(){
        onView(withId(R.id.flightRcv))
            .perform(RecyclerViewActions.actionOnItemAtPosition<FlightListAdapter.FlightViewHolder>(2,click()))
            onView(withId(R.id.newsSiteTxt)).check(matches(withText("SpaceNews")))
    }

    @Test
    fun test_backNavigation_toFlightListFragment(){
        onView(withId(R.id.flightRcv))
            .perform(RecyclerViewActions.actionOnItemAtPosition<FlightListAdapter.FlightViewHolder>(2,click()))
        onView(withId(R.id.newsSiteTxt)).check(matches(withText("SpaceNews")))
        pressBack()
        onView(withId(R.id.flightRcv)).check(matches(isDisplayed()))
    }

    @After
    fun tearDown() {
        IdlingRegistry.getInstance().unregister(EspressoIdlinResource.countingIdlingResource)
    }
}