package com.spaceflight

import com.domain.model.FlightDto
import com.data.mapper.Flight

class Mapper {
    fun toMapper(data: FlightDto): Flight {

        return Flight(
            title = data.title ?: "",
            imageUrl = data.imageUrl ?: "",
            summary = data.summary ?: "",
            id = data.id,
            newsSite = data.newsSite ?: ""

        )
    }

    fun FlightDto.toFlight(): Flight {
        return Flight(
            title = title,
            imageUrl = imageUrl,
            summary = summary,
            id = id,
            newsSite = newsSite

        )
    }
}