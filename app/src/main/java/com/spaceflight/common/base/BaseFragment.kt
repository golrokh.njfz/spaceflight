package com.spaceflight.common.base

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment


abstract class BaseFragment : Fragment() {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        clickListeners()
        subscribeViews()
        executeInitialTasks()
    }

    abstract fun executeInitialTasks()

    abstract fun subscribeViews()

    abstract fun clickListeners()

    abstract fun initView()

}