package com.spaceflight.common.utils

import androidx.test.espresso.idling.CountingIdlingResource


object EspressoIdlinResource {

    private const val RESOURCE = "GLOBAL"

    @JvmStatic
    val countingIdlingResource = CountingIdlingResource(RESOURCE)

    fun increment() {
        countingIdlingResource.increment()
    }

    fun decrement() {

        if (!countingIdlingResource.isIdleNow) {
            countingIdlingResource.decrement()
        }
    }
}