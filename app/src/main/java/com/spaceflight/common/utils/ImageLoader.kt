package com.spaceflight.common.utils

import android.annotation.SuppressLint
import android.content.Context
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.load.model.Headers
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade
import com.bumptech.glide.request.Request
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.*
import com.bumptech.glide.request.target.Target
import com.bumptech.glide.request.transition.Transition
import com.spaceflight.R


@GlideModule
class AppGlideModule : AppGlideModule()

object ImageLoaderHelper {

    private const val CARD_CURVE_SIZE = 4

    @SuppressLint("CheckResult")
    fun load(
        context: Context?,
        url: String?,
        imageView: ImageView,
        width: Int? = null,
        height: Int? = null,
        anchor: String = context?.getString(R.string.anchor_value).toString()
    ) {
        context?.let {
            var path = url
            val glideReq = GlideApp.with(context)
                .load( path.toString() )
                .transform(CenterCrop(),RoundedCorners((CARD_CURVE_SIZE.toPx())))
                .transition(withCrossFade())
            glideReq.into(imageView)
            width?.let { w ->
                height?.let { h ->
                    path = path.plus("?anchor=$anchor&crop=auto&scale=both&w=$w&h=$h")
                }
            }
        }
    }
}
