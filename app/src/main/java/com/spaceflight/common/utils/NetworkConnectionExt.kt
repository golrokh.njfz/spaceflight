import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.net.NetworkInfo
import android.os.Build

fun Context.isNetworkAvailable(): Boolean {
    getSystemService(Context.CONNECTIVITY_SERVICE).takeIf { it is ConnectivityManager }?.let {
        it as ConnectivityManager
    }?.let { connectivityManager ->
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            val capabilities = connectivityManager.getNetworkCapabilities(
                connectivityManager.activeNetwork
            )
            if (capabilities != null) {
                return when {
                    capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> {
                        true
                    }
                    capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> {
                        true
                    }
                    capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> {
                        true
                    }
                    else -> {
                        false
                    }
                }
            } else {
                return false
            }
        } else {
            try {
                val activeNetworkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo
                if (activeNetworkInfo != null && activeNetworkInfo.isConnected) {
                    return true
                }
            } catch (e: Exception) {
                return false
            }
        }
    }
    return false
}