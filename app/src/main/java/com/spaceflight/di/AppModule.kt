package com.spaceflight.di

import android.app.Application
import com.data.remote.network.api.FlightsApi
import com.data.repositories.FlightRepositoryImp
import com.domain.repository.FlightRepository
import com.domain.usecase.GetFlightListUseCase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class AppModule {
    private  val TIME_OUT = 30L
    private  val BASE_URL = "https://api.spaceflightnewsapi.net/v3/"
    @Singleton
    @Provides
    fun createOkHttpClient(): OkHttpClient {
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        return OkHttpClient.Builder()
            .connectTimeout(TIME_OUT, TimeUnit.SECONDS)
            .readTimeout(TIME_OUT, TimeUnit.SECONDS)
            .addInterceptor(httpLoggingInterceptor).build()
    }

    @Singleton
    @Provides
    fun createRetrofit(okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create()).build()
    }

    @Singleton
    @Provides
    fun createService(retrofit: Retrofit): FlightsApi {
        return retrofit.create(FlightsApi::class.java)
    }

    @Singleton
    @Provides
    fun createPostRepository(flightsApi: FlightsApi): FlightRepository {
        return FlightRepositoryImp(flightsApi)
    }
    @Singleton
    @Provides
    fun createGetFlightListUseCase(flightRepository: FlightRepository): GetFlightListUseCase {
    return GetFlightListUseCase(flightRepository)
}
}