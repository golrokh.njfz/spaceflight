package com.spaceflight.di

import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

private const val TIME_OUT = 30L
private const val BASE_URL = "https://api.spaceflightnewsapi.net/v3/"

//@Singleton
//@Provides
//fun createOkHttpClient(): OkHttpClient {
//    val httpLoggingInterceptor = HttpLoggingInterceptor()
//    httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BASIC
//    return OkHttpClient.Builder()
//        .connectTimeout(TIME_OUT, TimeUnit.SECONDS)
//        .readTimeout(TIME_OUT, TimeUnit.SECONDS)
//        .addInterceptor(httpLoggingInterceptor).build()
//}
//
//@Singleton
//@Provides
//fun createRetrofit(okHttpClient: OkHttpClient): Retrofit {
//    return Retrofit.Builder()
//        .baseUrl(BASE_URL)
//        .client(okHttpClient)
//        .addConverterFactory(GsonConverterFactory.create()).build()
//}
