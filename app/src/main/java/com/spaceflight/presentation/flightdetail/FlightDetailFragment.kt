package com.spaceflight.presentation.flightdetail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.spaceflight.common.base.BaseFragment
import com.spaceflight.common.utils.ImageLoaderHelper
import com.spaceflight.databinding.FragmentFlightDetailBinding
import kotlinx.android.synthetic.main.fragment_flight_detail.*


class FlightDetailFragment : BaseFragment() {

    private var _binding: FragmentFlightDetailBinding? = null
    private val binding get() = _binding
    private val args: FlightDetailFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentFlightDetailBinding.inflate(inflater, container, false)
        return binding?.root    }

    override fun executeInitialTasks() {

    }

    override fun subscribeViews() {
        binding?.titleTxt?.text = args.flight.title
        binding?.summaryTxt?.text = args.flight.summary
        binding?.flightImg?.let {
            ImageLoaderHelper.load(
                context = context,
                imageView = it,
                url = args.flight.imageUrl,
            )
        }
        binding?.newsSiteTxt?.text = args.flight.newsSite

    }

    override fun clickListeners() {
        backTxt.setOnClickListener {
            findNavController().popBackStack()
        }
    }

    override fun initView() {
    }


    companion object {
        @JvmStatic
        fun newInstance() = FlightDetailFragment()
    }
}