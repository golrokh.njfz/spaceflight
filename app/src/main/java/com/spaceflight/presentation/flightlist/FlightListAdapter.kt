package com.spaceflight.presentation.flightlist

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.data.mapper.Flight
import com.spaceflight.R
import com.spaceflight.common.utils.ImageLoaderHelper
import com.spaceflight.databinding.FlightItemCardBinding
import kotlinx.android.synthetic.main.flight_item_card.view.*
import javax.inject.Inject

 class FlightListAdapter @Inject constructor(private val itemSelect: ((Flight) -> Unit)) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {




    private var mFlightList = mutableListOf<Flight>()


override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
    val itemBinding = FlightItemCardBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            itemBinding.root.setOnClickListener {
            it.tag?.let { tag ->
                if (tag is Flight) {
                    itemSelect(tag)
                }
            }
        }
    return FlightViewHolder(itemBinding)
}

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as? FlightViewHolder)?.onBind(mFlightList[position])
    }

    override fun getItemCount(): Int {
        return mFlightList.size
    }

    inner class FlightViewHolder(private val viewDataBinding: FlightItemCardBinding) : RecyclerView.ViewHolder(viewDataBinding.root) {
        fun onBind(flight: Flight) {
            viewDataBinding.flightImg?.let {
                ImageLoaderHelper.load(
                    context = viewDataBinding.root.context,
                    imageView = it,
                    url = flight.imageUrl,
                )
            }
            viewDataBinding.titleTxt.text = flight.title
            viewDataBinding.root.tag = flight
        }

    }

    @SuppressLint("NotifyDataSetChanged")
    fun addAll(flightList: List<Flight>) {
        mFlightList.clear()
        mFlightList.addAll(flightList)
        notifyDataSetChanged()
    }
}