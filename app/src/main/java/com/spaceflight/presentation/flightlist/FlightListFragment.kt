package com.spaceflight.presentation.flightlist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.data.mapper.Flight
import com.spaceflight.R
import com.spaceflight.common.base.BaseFragment
import com.spaceflight.databinding.FragmentFlightListBinding
import dagger.hilt.android.AndroidEntryPoint
import isNetworkAvailable

@AndroidEntryPoint
class FlightListFragment : BaseFragment() {

    private val flightListViewModel: FlightListViewModel by viewModels()
    private var flightListAdapter: FlightListAdapter? = null
    private var _binding: FragmentFlightListBinding? = null//defining the binding class
    private val binding get() = _binding


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentFlightListBinding.inflate(inflater, container, false)
        return binding?.root

    }


    override fun initView() {
        if (context?.isNetworkAvailable() == true) {
            flightListViewModel.getFlightList()
        } else {
            Toast.makeText(
                context,
                getString(R.string.internet_connection),
                Toast.LENGTH_SHORT
            ).show()
        }

    }

    override fun executeInitialTasks() {
    }

    override fun subscribeViews() {
        flightListViewModel.showProgressbar.observe(viewLifecycleOwner) {
            if (it == true) {
                binding?.progressCircular?.visibility = View.VISIBLE
            } else {
                binding?.progressCircular?.visibility = View.GONE
            }
        }

        flightListViewModel.flightList.observe(viewLifecycleOwner) { flights ->
            binding?.progressCircular?.visibility = View.GONE
            flightListAdapter = FlightListAdapter { flightCallBack ->
                val action =
                    FlightListFragmentDirections.actionDestinationFlightListToDestinationFlightDetail(
                        flightCallBack
                    )
                findNavController().navigate(action)
            }
            setUpAdapter(flightList = flights)
        }
        flightListViewModel.messageData.observe(this) {
            Toast.makeText(context, it, Toast.LENGTH_LONG).show()
        }
    }

    override fun clickListeners() {
    }


    private fun setUpAdapter(flightList: List<Flight>) {
        flightList.let {
            flightListAdapter?.addAll(it)
        }
        binding?.flightRcv?.adapter = flightListAdapter
        binding?.flightRcv?.layoutManager = LinearLayoutManager(context)
    }

    companion object {
        @JvmStatic
        fun newInstance() = FlightListFragment()
    }
}