package com.spaceflight.presentation.flightlist

import androidx.lifecycle.*
import com.domain.usecase.GetFlightListUseCase
import com.data.mapper.Flight
import com.domain.common.Result.*
import com.spaceflight.Mapper
import com.spaceflight.common.utils.EspressoIdlinResource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject


@HiltViewModel
class FlightListViewModel @Inject constructor(private val flightListUseCase: GetFlightListUseCase) :
    ViewModel() {

    val showProgressbar = MutableLiveData<Boolean>()
    private val _flightList = MutableLiveData<List<Flight>>()
    val flightList: LiveData<List<Flight>> = _flightList
    private val _messageData = MutableLiveData<String>()
    val messageData : LiveData<String> = _messageData

    fun getFlightList() {
        showProgressbar.value = true
        viewModelScope.launch {
            EspressoIdlinResource.increment()

            when (val flightsResult = flightListUseCase.invoke()) {
                is Success -> {
                    EspressoIdlinResource.decrement()

                    showProgressbar.value = false
                    _flightList.value = flightsResult.data?.map {
                        Mapper().toMapper(it)
                    }
                }
                is Error -> {
                    _messageData.value = flightsResult.exception.message
                    showProgressbar.value = false
                }
            }


        }
    }
}