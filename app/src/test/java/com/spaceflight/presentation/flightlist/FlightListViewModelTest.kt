package com.spaceflight.presentation.flightlist

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.data.mapper.Flight
import com.domain.common.Result
import com.domain.model.FlightDto
import com.domain.repository.FlightRepository
import com.domain.usecase.GetFlightListUseCase
import com.google.common.truth.Truth.assertThat
import com.spaceflight.MainCoroutineRule
import getOrAwaitValue
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import java.lang.Exception
import org.mockito.Mockito.`when` as whenever


@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class FlightListViewModelTest {

    @get:Rule
    val testInstantTaskExecutorRule: TestRule = InstantTaskExecutorRule()
    @get:Rule
    val testCoroutineRule = MainCoroutineRule()
    var viewModel: FlightListViewModel?=null
    @Mock
    private lateinit var flightRepository: FlightRepository


    @Before
    fun setUp() {
        viewModel = FlightListViewModel(GetFlightListUseCase(flightRepository))
    }



    @After
    fun tearDown() {

viewModel = null
    }
    @Test
    fun `when fetching results ok then return a list successfully`() {

        testCoroutineRule.runBlockingTest {
            whenever(flightRepository.getFlights()).thenAnswer {
                Result.Success(listOf(FlightDto("1", "", "title", "summary", "NASA")))
            }
            viewModel?.getFlightList()
            val result = viewModel?.flightList?.getOrAwaitValue()

            assertThat(result).isEqualTo(result?.map {
                Flight(
                    title = it.title,
                    imageUrl = it.imageUrl,
                    summary = it.summary,
                    id = it.id,
                    newsSite = it.newsSite
                )
            })

        }
    }

    @Test
    fun `when fetching results fails then return an error`() {
        testCoroutineRule.runBlockingTest {
           whenever(flightRepository.getFlights()).thenAnswer {
                Result.Error(10, Exception("unAuthorized") )
            }
            viewModel?.getFlightList()
            val apiResult = viewModel?.messageData?.getOrAwaitValue()
            assertEquals(apiResult, "unAuthorized")

        }
    }
}



