package com.data.mapper

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Flight(
    val id: String?,
    val imageUrl: String?,
    val title: String?,
    val summary: String?,
    val newsSite: String?
):Parcelable
