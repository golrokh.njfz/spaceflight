package com.data.remote.network.api

import com.domain.model.FlightDto
import retrofit2.Response
import retrofit2.http.GET

interface FlightsApi {

    @GET("articles?_limit=7")
    suspend fun getFlightList():Response<List<FlightDto>>
}