package com.data.repositories

import com.domain.common.Result
import retrofit2.Response

open class BaseRepository {
    fun <T> execute(response: Response<T>): Result<T?> {
        return try {
            if (response.isSuccessful) {
                Result.Success(response.body())
            } else {
                Result.Error(response.code()?: 0,Exception(response.message()))
            }
        } catch (e: Exception) {
            Result.Error(response.code(),e)
        }
    }
}