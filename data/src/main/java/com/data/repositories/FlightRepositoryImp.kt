package com.data.repositories

import com.data.remote.network.api.FlightsApi
import com.domain.common.Result
import com.domain.model.FlightDto
import com.domain.repository.FlightRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class FlightRepositoryImp @Inject constructor (private val flightsApi: FlightsApi
   ):FlightRepository,BaseRepository() {
    override suspend fun getFlights(): Result<List<FlightDto>?>  = execute(flightsApi.getFlightList())
}