package com.domain.model

data class FlightDto(
    val id: String,
    val imageUrl: String,
    val title: String,
    val summary: String?,
    val newsSite: String?

)