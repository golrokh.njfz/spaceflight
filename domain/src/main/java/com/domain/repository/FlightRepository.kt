package com.domain.repository

import com.domain.common.Result
import com.domain.model.FlightDto

interface FlightRepository  {
   suspend fun getFlights(): Result<List<FlightDto>?>
}