package com.domain.usecase

import com.domain.model.FlightDto
import com.domain.repository.FlightRepository
import com.domain.usecase.base.UseCase
import javax.inject.Inject

class GetFlightListUseCase @Inject constructor(private val flightRepository: FlightRepository) {
        suspend operator fun invoke() = flightRepository.getFlights()
}